var nameList = require('./names');

exports.randomDefaultMessage = function randomDefaultMessage (){
	var msgRep = ['Não sei responder a isso, ainda estou a aprender :)','Isso não sei mas pergunta outras coisas :)', 'Ups mas só o Google sabe tudo',];
	var msg = msgRep[Math.floor(Math.random() * msgRep.length)];
	return msg
};
exports.getPresence = function getPresence(name){
	var r = '\\b' + name ;
	var tester = new RegExp(r);
	return nameList.names.filter(function(a){return tester.test(a.toLocaleLowerCase()) })

};

exports.randomPresenceMessage = function randomDefaultMessage (){
	var msgRep = ['Claro que vai ;)','Não falha uma ;)', 'Verifiquei a minha lista: CHECK!'];
	var msg = msgRep[Math.floor(Math.random() * msgRep.length)];
	return msg
};

exports.randomNoNameMessage = function randomDefaultMessage (){
	var msgRep = ['Preciso de um nome para saber isso :)', 'Não consigo fazer queries sem parametros'];
	var msg = msgRep[Math.floor(Math.random() * msgRep.length)];
	return msg
};
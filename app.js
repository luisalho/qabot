var restify = require('restify');
var builder = require('botbuilder');
var cognitiveservices = require('botbuilder-cognitiveservices');
var appInsights = require('applicationinsights');
var instrumentation = require('botbuilder-instrumentation');
var methods = require('./methods');
var names = require('./names');

//=========================================================
// Bot Setup
//=========================================================

// Setting up advanced instrumentation
let logging = new instrumentation.BotFrameworkInstrumentation({
	instrumentationKey: process.env.APPINSIGHTS_INSTRUMENTATIONKEY
});
// Setting up app Insights Client 

appInsights.setup(process.env.APPINSIGHTS_INSTRUMENTATIONKEY).start();
var appInsightsClient = new appInsights.TelemetryClient();

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
	console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
var connector = new builder.ChatConnector({
	appId: process.env.MICROSOFT_APP_ID,
	appPassword: process.env.MICROSOFT_APP_PASSWORD
});

var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Bots Dialogs
//=========================================================

var recognizer = new builder.LuisRecognizer(process.env.LUISMODELURL)

var QArecognizer = new cognitiveservices.QnAMakerRecognizer({
	knowledgeBaseId: process.env.QAMAKER_ID,
	subscriptionKey: process.env.QAMAKER_KEY
})
	;

// var basicQnAMakerDialog = new cognitiveservices.QnAMakerDialog({
// 	recognizers: [recognizer],
// 	defaultMessage: randomDefaultMessage(),
// 	qnaThreshold: 0.3
// });

var intents = new builder.IntentDialog({
	recognizers: [QArecognizer, recognizer],
	recognizeOrder: 'series',
	intentThreshold: 0.65
});
bot.dialog('/', intents);

intents.matches('qna', [
	function (session, args, next) {
		session.conversationData.answerCounter = 0;
		var answerEntity = builder.EntityRecognizer.findEntity(args.entities, 'answer');
		session.send(answerEntity.entity);

	}
]);

bot.beginDialogAction('asneiras', '/asneiras', { matches: /^(merda|foda-se|puta|caralho|picha|cona|putona|putéfia|conão|caralhão|blyat|блять)/i });
bot.dialog('/asneiras', [function (session, args, next) {
	session.endConversation('Ai! Ai! Ai! Nada de asneiras')
}]);

intents.matches('presence', [
	function (session, args, next) {
		if (args.entities.length > 0) {
			var name = ""
			args.entities.forEach(function (element) {

				name = name + ' ' + element.resolution.values[0];

			}, this);
		
		name = name.substring(1);
		var names = methods.getPresence(name);


		if (names.length == 1) {
			session.endConversation(methods.randomPresenceMessage());
		}
		else if (names.length > 1) {
			var msg = "";
			session.send("Com esse nome vão:")
			names.forEach(function (element) {
				session.send(element);
			}, this);
			session.send("Só querem festa!")
		}
		else {
			session.endConversation('Não vai niguém com esse nome :(');
		}
	}
	else {
		session.endConversation(methods.randomNoNameMessage());
	}
	}
]);

intents.onDefault([
	function (session) {
		session.conversationData.answerCounter = session.conversationData.answerCounter + 1;
		if (session.conversationData.answerCounter > 2 && session.conversationData.answerCounter < 4) {
			session.send('Já paravas de perguntar coisas que não sei');
		}
		else if (session.conversationData.answerCounter > 3) {
			session.send('Estou com dificuldades em responder às tuas questões.')^
			session.send('Experimenta perguntas do tipo: \r\nOnde é o jantar? \r\nA que horas é o jantar? \r\nO que é o jantar?')
		}
		else {
			session.send(methods.randomDefaultMessage());
		}
	}
]);

//bot.dialog('/', basicQnAMakerDialog);

logging.monitor(bot, recognizer);